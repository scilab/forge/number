// Copyright (C) 2009 - Digiteo - Michael Baudin

//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// Factor large numbers
//
function benchmarkFactor()
    mprintf("\nFactor large numbers\n" )
    function logger ( msg )
        mprintf ( "%s\n" , msg )
        mfprintf ( loghandle , "%s\n", msg )
    endfunction  

    function factorall ( numberlist , method )
        t1 = timer();
        logfile = "factor.log.txt"
        mprintf("Method = %s\n" , method )
        mprintf("Writing logfile: %s\n",logfile)
        loghandle = mopen( "factor.log.txt" , "w" );

        logger ( sprintf ( "Method = %s\n" , method ) )
        total = 0;
        success = 0;
        failure = 0;
        for i = numberlist
            total = total + 1;
            logger ( sprintf ( "==================\n" ) )
            mprintf("Factoring n=2^%d-1 \n" , i )
            logger ( sprintf ( "Factoring n=2^%d-1 \n" , i ) )
            n = 2^i - 1
            [ computed , status ] = number_factor ( n , method );
            if ( status ) then
                success = success + 1;
                logger ( sprintf ( "> Success\n" ) )
                logger ( sprintf ( "2^%d - 1 = %s\n" , i , strcat ( string ( computed ) , "." ) ) )
            else
                failure = failure + 1;
                logger ( sprintf ( "> Failure !!!\n" ) )
                logger ( sprintf ( "2^%d - 1 = %s\n" , i , strcat ( string ( computed ) , "." ) ) )
            end
            // Check that factors are prime
            for p = computed
                isprime = number_isprime ( p )
                if ( ~isprime ) then
                    logger ( sprintf ( "> Factor %d is not prime\n" , p ) )
                end
            end
        end
        t2 = timer();

        logger ( sprintf ( "=====================\n" ) )
        logger ( sprintf ( "Total factorings : %d\n" , total ) )
        logger ( sprintf ( "Success : %d\n" , success ) )
        logger ( sprintf ( "Failures : %d\n" , failure ) )
        logger ( sprintf ( "Time : %f (s)\n" , t2 - t1 ) )

        mclose( loghandle );
    endfunction

    numberlist = 10:14
    factorall ( numberlist , "fasttrialdivision" )
    factorall ( numberlist , "trialdivision" )
    factorall ( numberlist , "memorylesstd" )
    factorall ( numberlist , "fermat" )
    factorall ( numberlist , "pollard-rho" )

endfunction 
benchmarkFactor();
clear benchmarkFactor


