// This help file was automatically generated using helpupdate
// PLEASE DO NOT EDIT
demopath = get_absolute_file_path("number.dem.gateway.sce");
subdemolist = [
"benchmark", "benchmark.sce"; ..
"factor", "factor.sce"; ..
"factor_largenumbers", "factor_largenumbers.sce"; ..
"factor_mersenne", "factor_mersenne.sce"; ..
"primegapplot", "primegapplot.sce"; ..
"primegapdistribution", "primegapdistribution.sce"; ..
"searchinglargeprimes", "searchinglargeprimes.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
