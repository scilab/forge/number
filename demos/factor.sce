// Copyright (C) 2009 - Digiteo - Michael Baudin

//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// Factor numbers from 1 to N
//
mprintf("Factor numbers from 1 to N\n");
function benchmarkFactor()
    function logger ( msg )
        mprintf ( "%s\n" , msg )
        mfprintf ( loghandle , "%s\n", msg )
    endfunction  

    function factorall ( nmax , method )
        t1 = timer();
        logfile = "factor.log.txt"
        factorsfile = "factors.txt"
        mprintf("Writing logfile: %s\n",logfile)
        mprintf("Writing factorsfile: %s\n",factorsfile)
        loghandle = mopen( logfile , "w" );
        factorhandle = mopen( factorsfile , "w" );

        logger ( sprintf ( "Method = %s\n" , method ) )
        total = 0;
        success = 0;
        failure = 0;
        for n = 1 : nmax
            total = total + 1;
            //mfprintf ( loghandle , "Factoring #%d / %d\n" , n , nmax )
            logger ( sprintf ( "Factoring n=%d / %d\n" , n , nmax ) )
            [ computed , status ] = number_factor ( n , method );
            if ( status ) then
                success = success + 1;
                logger ( sprintf ( "> Success\n" ) )
                mfprintf ( factorhandle , "%d = %s\n" , ..
                n , strcat ( string ( computed ) , "." ) )
            else
                failure = failure + 1;
                logger ( sprintf ( "> Failure !!!\n" ) )
                mfprintf ( factorhandle , "%d = %s (Incomplete)\n" , ..
                n , strcat ( string ( computed ) , "." ) )
            end
            // Check that factors are prime
            for p = computed
                isprime = number_isprime ( p )
                if ( ~isprime ) then
                    logger ( sprintf ( "> Factor %d is not prime\n" , p ) )
                end
            end
        end
        t2 = timer();

        logger ( sprintf ( "=====================\n" ) )
        logger ( sprintf ( "Total factorings : %d\n" , total ) )
        logger ( sprintf ( "Success : %d\n" , success ) )
        logger ( sprintf ( "Failures : %d\n" , failure ) )
        logger ( sprintf ( "Time : %f (s)\n" , t2 - t1 ) )

        mclose( loghandle );
        mclose( factorhandle );
    endfunction

    factorall ( 20 , "fasttrialdivision" )
    factorall ( 20 , "trialdivision" )
    factorall ( 20 , "memorylesstd" )
    factorall ( 20 , "fermat" )
    factorall ( 10 , "pollard-rho" )

endfunction

benchmarkFactor();
clear benchmarkFactor
