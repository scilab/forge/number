// Copyright (C) 2009 - Digiteo - Michael Baudin

//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// Test performances
//

function numberBenchmark()
    mprintf("Benchmarking performances of the number module\n");
    //===================================================
    //
    // Test number_isprime
    // M31 = 2^31-1 is a prime number
    //
    function benchmark_isprime ( method )
        imax = 100000
        n = 2^31-1
        mprintf("M31 = 2^31-1 is a prime number\n")
        mprintf("number_isprime(%d,""%s"")...\n",n,method)
        for i = 1:5
            tic;
            isprime = number_isprime( n , method , imax );
            t = toc()
            mprintf ( "    Run #%d: t=%f (s)\n",i,t)
        end
    endfunction
    // "modulo" 0.588000 0.589000 0.586000 0.592000 0.588000
    benchmark_isprime ( "modulo" )
    // "6k"   0.557000 0.599000 0.557000 0.560000 0.555000
    benchmark_isprime ( "6k" )
    // 30k = 0.123000 0.123000 0.122000 0.122000 0.122000
    benchmark_isprime ( "30k" )
    // 210k = 0.021000 0.020000 0.020000 0.020000 0.020000
    benchmark_isprime ( "210k" )
    // "massdivide" = 0.003000 0.002000 0.002000 0.004000 0.002000
    benchmark_isprime ( "massdivide" )
    // With "primes", the required memory is too large...
    // Conclusion "6k" is faster
    //===================================================
    //
    // Test number_primes
    //
    function benchmark_primes ( method )
        mprintf("number_primes(10000,""%s"")...\n",method)
        for i = 1:5
            tic;
            computed = number_primes ( 10000 , method );
            t = toc()
            mprintf ( "    Run #%d: t=%f (s)\n",i,t)
        end
    endfunction
    // With fast erathostene
    // 0.010000 0.060000 0.000000 0.050000 0.010000
    // 0.070000 0.010000 0.010000 0.060000 0.000000
    benchmark_primes ( "erathostenefast" )
    // Test with naive erathostene
    // 0.781000 0.801000 0.782000 0.771000 0.771000
    // 0.681000 0.761000 0.831000 0.742000 0.761000
    benchmark_primes ( "erathostenenaive" )
    // Conclusion : "fast" is faster...
    //===================================================
    //
    // Test number_gcd
    // 
    mprintf("2147483647 = Mersenne Prime\n")
    mprintf("370248451 = Lucas Prime\n")
    function benchmark_gcd ( method )
        mprintf("number_gcd ( 2147483647 , 370248451 , ""%s"" )...\n",method)
        for i = 1:5
            tic;
            for j = 1:100
                computed = number_gcd ( 2147483647 , 370248451 , method );
            end
            t = toc()
            mprintf ( "    Run #%d: t=%f (s)\n",i,t)
        end
    endfunction
    // With "euclidian" =  0.190000 0.210000 0.231000 0.200000 0.210000
    benchmark_gcd ( "euclidian" )
    //benchmark_gcd ( "brute" ) really too slow...
    benchmark_gcd ( "binary" )
    // binary = 1.231000 1.161000 1.252000 1.222000 1.352000
    benchmark_gcd ( "euclidianiterative" )
    // "euclidianiterative" = 0.270000 0.220000 0.251000 0.260000 0.281000
    benchmark_gcd ( "binaryiterative" )
    // "binaryiterative" = 0.972000 0.851000 0.891000 0.922000 0.981000
    // Conclusion : recursive Euclidian is faster

    //===================================================
    //
    // Test number_factor

    // Factors an integer
    function benchmark_factor ( n , method )
        mprintf("number_factor ( %s , ""%s"" )...\n",string(n),method)
        for i = 1:5
            tic;
            instr = "computed = number_factor ( n , method )";
            ierr=execstr(instr,'errcatch')
            msg = lasterror()
            t = toc()
            if ( ierr==0 ) then
                mprintf ( "    Run #%d: t=%f (s)\n",i,t)
            else
                mprintf ( "    Error.\n")
            end
        end
    endfunction
    //  "trialdivision"  : too much memory ...
    //benchmark_factor ( "trialdivision" )
    // "fermat" : 0.131000 0.129000 0.130000 0.130000 0.131000
    mprintf("Test with large number and small factors\n")
    mprintf("2^52 - 1 = 3.5.53.157.1613.2731.8191\n")
    n = 2^52 - 1;
    benchmark_factor ( n , "fermat" )
    benchmark_factor ( n , "memorylesstd" )
    // "memorylesstd" : 0.205000 0.204000 0.204000 0.204000 0.205000
    // Winner : "fermat"
    // Second : "memorylesstd"


    // Test with large number with large factors
    // 2^53 - 1 = 6361.69431.20394401
    //  "trialdivision"  : too much memory ...
    //benchmark_factor ( n, "trialdivision" )
    // "fermat" : too slow
    //benchmark_factor ( n, "fermat" )
    mprintf("Test with large number with large factors\n")
    mprintf("2^53 - 1 = 6361.69431.20394401\n")
    n = 2^53 - 1
    benchmark_factor ( n, "memorylesstd" )

    // Test with small number with small factors
    // 2^20 - 1 = 3.5.5.11.31.41
    //  "trialdivision"  : 0.200000 0.201000 0.210000 0.200000 0.221000
    mprintf("Test with small number with small factors\n")
    mprintf("2^20 - 1 = 3.5.5.11.31.41\n")
    n = 2^20 - 1
    benchmark_factor ( n, "trialdivision" )
    //  "fasttrialdivision"  : 0.040000 0.100000 0.091000 0.070000 0.040000
    benchmark_factor ( n, "fasttrialdivision" )
    // "fermat" : 0.050000 0.120000 0.100000 0.080000 0.091000
    benchmark_factor ( n, "fermat" )

    // Other tests :
    // 2^50 - 1 = 3.11.31.251.601.1801.4051
    // number_factor ( 2^50 - 1 )
    // trialdivision : too much memory
    // fermat : too slow

    // 2^51 - 1 = 7.103.2143.11119.131071
    // fermat : too slow

    // Conclusion : 
    // trialdivision fails everywhere (too much memory)
    // fermat is very slow on 2^51-1 and 2^53-1 : factors are not enough close
    // pollard fails on 2^51-1 : 7.    321685687669321.
    // Conclusion : we do not have a good algorithm yet...
endfunction 
numberBenchmark();
clear numberBenchmark

