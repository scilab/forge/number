// Copyright (C) 2009 - Digiteo - Michael Baudin

//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// Factor Mersenne Numbers ( 2^n - 1 ) with n = 1:10
//
function benchmarkFactor()
    mprintf("Factor Mersenne Numbers ( 2^n - 1 ) with n = 1:10\n")
    function logger ( msg )
        mprintf ( "%s\n" , msg )
        mfprintf ( loghandle , "%s\n", msg )
    endfunction  

    function s = number_tostring ( n )
        s = string ( n )
    endfunction

    function factormersenne ( )
        t1 = timer();
        loghandle = mopen( "factor_mersenne.log.txt" , "w" );

        total = 0;
        success = 0;
        failure = 0;
        methodlist = [
        "fasttrialdivision"
        "trialdivision"
        "memorylesstd"
        "fermat"
        "pollard-rho"
        ].';
        for i = 2 : 10
            n = 2^i - 1
            logger ( sprintf ( "==========================\n" ) )
            logger ( sprintf ( "Factoring M_%d = 2^%d - 1 = n=%s \n" , i , i , string(n) ) )
            for method = methodlist
                logger ( sprintf ( "Method = %s\n" , method ) )
                if ( ( method = "fasttrialdivision" ) & ( i >= 41 ) ) then 
                    logger ( sprintf ( "Skipping\n" ) )
                    continue
                elseif ( ( method = "trialdivision" ) & ( i >= 41 ) ) then 
                    logger ( sprintf ( "Skipping\n" ) )
                    continue
                end
                total = total + 1;
                [ computed , status ] = number_factor ( n , method );
                logger ( sprintf ( "%s = %s\n" , string(n) , strcat ( string ( computed ) , "." ) ) )
                if ( status ) then
                    success = success + 1;
                    logger ( sprintf ( "> Success\n" ) )
                else
                    failure = failure + 1;
                    logger ( sprintf ( "> Failure !!!\n" ) )
                end
                // Check that factors are prime
                for p = computed
                    isprime = number_isprime ( p )
                    if ( ~isprime ) then
                        logger ( sprintf ( "> Factor %s is not prime\n" , string(p) ) )
                    end
                end
            end
        end
        t2 = timer();

        logger ( sprintf ( "=====================\n" ) )
        logger ( sprintf ( "Total factorings : %d\n" , total ) )
        logger ( sprintf ( "Success : %d\n" , success ) )
        logger ( sprintf ( "Failures : %d\n" , failure ) )
        logger ( sprintf ( "Time : %f (s)\n" , t2 - t1 ) )

        mclose( loghandle );
    endfunction

    //
    // Run !!!
    //
    // Use 17 digits : these are large numbers...
    format(25)
    factormersenne ( );


endfunction
benchmarkFactor();
clear benchmarkFactor
