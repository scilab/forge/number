// Copyright (C) 2008 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt



hexstr = "3FE54B3504C6B4A3";
binstr = number_hex2bin (hexstr);
expected = "0011111111100101010010110011010100000100110001101011010010100011";
assert_checkequal ( binstr , expected );
//
hexstr = "XYZ";
instr="binstr = number_hex2bin (hexstr)";
ierr = execstr(instr,"errcatch");
lerr = lasterror();
assert_checkequal ( ierr , 10000 );
assert_checkequal ( lerr , "number_hex2bin: Letter X at entry #1 at indices (1,1) in the matrix is not hexadecimal." );
//
hexstr = "3fe54b3504c6b4a3";
binstr = number_hex2bin (hexstr);
expected = "0011111111100101010010110011010100000100110001101011010010100011";
assert_checkequal ( binstr , expected );

