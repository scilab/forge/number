// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->




// Check for actual primes
assert_checkequal ( number_probableprime ( 7 ) , %t );
assert_checkequal ( number_probableprime ( 5 ) , %t );
assert_checkequal ( number_probableprime ( 5 , 3 ) , %t );
// Check for composite numbers
assert_checkequal ( number_probableprime ( 10 ) , %f );
assert_checkequal ( number_probableprime ( 20 ) , %f );
// Check for pseudo-primes : while pseudo prime does not detect that these numbers
// are composite, probable prime does it perfectly.
assert_checkequal ( number_probableprime ( 341 ) , %f );
assert_checkequal ( number_probableprime ( 561 ) , %f );
assert_checkequal ( number_probableprime ( 645 ) , %f );
assert_checkequal ( number_probableprime ( 1105 ) , %f );

// Test verbose
number_probableprime ( 10001 , [] , %t );


// Test a few numbers
mprintf("Test a few numbers\n");
integerlist  = [1  2  3  4  5  6  7  8  9  10 73 32003];
expectedlist = [%f %t %t %f %t %f %t %f %f %f %t %t].';
computed = [];
for i=1:size(integerlist,"*")
    mprintf ( "i=%d\n", integerlist ( i ) )
    computed = number_probableprime ( integerlist ( i ) );
    expected = expectedlist ( i );
    assert_checkequal ( computed , expected );
end
// Test some even integers
mprintf("Test some even integers\n");
integerlist = 2 * ( 2 : 50 );
expected = %f;
for i=1:size(integerlist,"*")
    mprintf ( "i=%d\n", integerlist ( i ) )
    computed = number_probableprime ( integerlist ( i ) );
    assert_checkequal ( computed , expected );
end
// Test some primes
mprintf("Test some primes\n");
integerlist = [2,5,73,109,113,293,557,563,593,601,823,947,967];
expected = %t;
for i=1:size(integerlist,"*")
    mprintf ( "i=%d\n", integerlist ( i ) )
    computed = number_probableprime ( integerlist ( i ) );
    assert_checkequal ( computed , expected );
end

// Check a large prime : too large for us...
//assert_checkequal ( number_probableprime ( 4432676798593 ) , %f );

