// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->


// Test without options
computed(1) = number_gcd ( 84 , 18 );
computed(2) = number_gcd ( 18 , 84 );
expected = [6 6].';
assert_checkequal ( computed , expected );

// Test without options
computed(1) = number_gcd ( 84 , -18 );
computed(2) = number_gcd ( 18 , -84 );
expected = [6 6].';
assert_checkequal ( computed , expected );

// Test "euclidian"
computed(1) = number_gcd ( 84 , 18 , "euclidian" );
computed(2) = number_gcd ( 18 , 84 , "euclidian" );
expected = [6 6].';
assert_checkequal ( computed , expected );

// Test "brute"
computed(1) = number_gcd ( 84 , 18 , "brute" );
computed(2) = number_gcd ( 18 , 84 , "brute" );
expected = [6 6].';
assert_checkequal ( computed , expected );


// Test "binary"
computed(1) = number_gcd ( 84 , 18 , "binary" );
computed(2) = number_gcd ( 18 , 84 , "binary" );
expected = [6 6].';
assert_checkequal ( computed , expected );

// Test "euclidianiterative"
computed(1) = number_gcd ( 84 , 18 , "euclidianiterative" );
computed(2) = number_gcd ( 18 , 84 , "euclidianiterative" );
expected = [6 6].';
assert_checkequal ( computed , expected );

// Test "binaryiterative"
computed(1) = number_gcd ( 84 , 18 , "binaryiterative" );
computed(2) = number_gcd ( 18 , 84 , "binaryiterative" );
expected = [6 6].';
assert_checkequal ( computed , expected );

