// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

assert_checkequal ( number_fermat ( 0 ) , 3 );
assert_checkequal ( number_fermat ( 1 ) , 5 );
assert_checkequal ( number_fermat ( 2 ) , 17 );
assert_checkequal ( number_fermat ( 3 ) , 257 );
assert_checkequal ( number_fermat ( 4 ) , 65537 );
assert_checkequal ( number_fermat ( 5 ) , 641*6700417 );

