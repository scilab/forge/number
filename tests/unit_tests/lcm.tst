// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

// Test without options
computed(1) = number_lcm ( 4 , 6 );
computed(2) = number_lcm ( 6 , 4 );
expected = [12 12].';
assert_checkequal ( computed , expected );
// Test without options
computed(1) = number_lcm ( 4 , -6 );
computed(2) = number_lcm ( 6 , -4 );
expected = [12 12].';
assert_checkequal ( computed , expected );
//
// Test with euclidian
computed(1) = number_lcm ( 4 , 6 , "euclidian" );
computed(2) = number_lcm ( 6 , 4 , "euclidian" );
expected = [12 12].';
assert_checkequal ( computed , expected );
//
m = number_lcm ( 0 , 0 );
assert_checkequal ( m , 0 );
