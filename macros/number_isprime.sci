// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function varargout = number_isprime ( varargin )
  // Checks if a number is prime.
  //
  // Calling Sequence
  //   isprime = number_isprime ( n )
  //   [ isprime , status ] = number_isprime ( n )
  //   [ isprime , status ] = number_isprime ( n , method )
  //   [ isprime , status ] = number_isprime ( n , method , imax )
  //   [ isprime , status ] = number_isprime ( n , method , imax , verbose )
  //
  // Parameters
  // n : a 1x1 matrix of floating point integers, must be positive
  // method : a 1x1 matrix of strings, the algorithm to use, default method = "6k"
  // imax : a 1-by-1 matrix of floating point integers, the number of steps in the loop (default imax=10000)
  // verbose : a 1-by-1 matrix of booleans, set to true to display messages (default verbose=%f)
  // isprime : a 1x1 matrix of booleans, isprime=%t if n is prime, isprime=%f if not.
  // status : a 1x1 matrix of booleans, status=%t if the test performed completely, status=%f if the test failed
  //
  // Description
  //   Returns %t if the given number is prime.
  //   If status is %t, then
  //   <itemizedlist>
  //   <listitem> isprime is true if n is prime.</listitem>
  //   <listitem> isprime is false if n is not prime.</listitem>
  //   </itemizedlist>
  //   If status is %f, then the primality test failed.
  //
  //  Any input argument equal to the empty matrix is replaced by its default value.
  //
  // The following values of method are available.
  // <variablelist>
  //   <varlistentry>
  //      <term> "modulo" </term>
  //     <listitem>
  //     <para>
  //     Uses the modulo function.
  //     </para>
  //     </listitem>
  //   </varlistentry>
  //   <varlistentry>
  //     <term> "6k" </term>
  //     <listitem>
  //     <para>
  //     Based on modulo function and on the fact that prime
  //     numbers are of the form 6k + 1, 6k - 1.
  //     The sequence of divisors to test is :
  //     5, 7, 11, 13, 17, 19, ...
  //     </para>
  //     </listitem>
  //   </varlistentry>
  //   <varlistentry>
  //     <term> "30k" </term>
  //     <listitem>
  //     <para>
  //     Based on modulo function and on the fact that prime
  //     numbers are of the form 30k + i, with i = 1, 7, 11, 13, 17, 19, 23, 29.
  //     Notice that the primorial function for n = 5 is equal to c#(5) = 2*3*5 = 30
  //     The numbers 1,7,11,...,29 are the ones so that gcd(i,30)=1
  //     We must first test for n = 2, 3 or 5.
  //     </para>
  //     </listitem>
  //   </varlistentry>
  //   <varlistentry>
  //     <term> "210k" </term>
  //     <listitem>
  //     <para>
  //     Based on modulo function and on the fact that prime
  //     numbers are of the form 210k + i, with i = 1 11 13 17 ... 179 181 187 191 193 197 199 209
  //     We have c#(7) = 210.
  //     The numbers 1,11,...,209  are the ones so that gcd(i,210)=1
  //     We must first test for n = 2, 3 or 5 or 7.
  //     This test may perform well (i.e. fast) in the case where the number is
  //     very large.
  //     </para>
  //     </listitem>
  //   </varlistentry>
  //   <varlistentry>
  //     <term> "primes" </term>
  //     <listitem>
  //     <para>
  //     Based on <literal>number_primes</literal> function : if the
  //     last number is equal to <literal>n</literal>, then the
  //     number is prime.
  //     </para>
  //     </listitem>
  //   </varlistentry>
  //   <varlistentry>
  //     <term> "massdivide" </term>
  //     <listitem>
  //     <para>
  //     Based on a massive division of n by all odd numbers up to sqrt(n).
  //     This is fast, but requires all lot of memory if the number is large.
  //     </para>
  //     </listitem>
  //   </varlistentry>
  // </variablelist>
  //
  // Examples
  // // Check simple examples
  // isprime = number_isprime(0) // %f
  // isprime = number_isprime(1) // %f
  // isprime = number_isprime(2) // %t
  // isprime = number_isprime(7) // %t
  // isprime = number_isprime(10) // %f
  //
  // // Configure imax
  // // Success:
  // isprime = number_isprime(1013,[],1000)
  // // Generates an error (not enough iterations):
  // isprime = number_isprime(1013,[],2)
  //
  // // Use verbose option
  // number_isprime(1013,[],[],%t)
  //
  // // A loop on some values
  // algos = ["primes"
  //   "modulo"
  //   "6k"
  //   "30k"
  //   "210k"
  //   "massdivide"
  //   ]
  // mprintf("   primes, modulo, 6k, 30k, 210k, massdivide\n");
  // for n = 1 : 20
  //   msg="";
  //   msg=msg+msprintf("n=%d ",n);
  //   for method = algos'
  //     isprime = number_isprime(n,method);
  //     msg=msg+msprintf("%s ",string(isprime));
  //   end
  //   msg=msg+msprintf("\n");
  //   mprintf("%s\n",msg);
  // end
  //
  // Authors
  // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
  //

  [lhs,rhs]=argn();
  apifun_checkrhs ( "number_isprime" , rhs , 1:4 )
  apifun_checklhs ( "number_isprime" , lhs , 1:2 )
  //
  // Get arguments
  n = varargin ( 1 )
  method = apifun_argindefault (  varargin , 2 , "6k" )
  imax = apifun_argindefault (  varargin , 3 , 10000 )
  verbose = apifun_argindefault (  varargin , 4 , %f )
  //
  // Check type
  apifun_checktype ( "number_isprime" , n , "n" , 1 , "constant" )
  apifun_checktype ( "number_isprime" , method , "method" , 2 , "string" )
  apifun_checktype ( "number_isprime" , imax , "imax" , 3 , "constant" )
  apifun_checktype ( "number_isprime" , verbose , "verbose" , 4 , "boolean" )
  //
  // Check size
  apifun_checkscalar ( "number_isprime" , n , "n" , 1 )
  apifun_checkscalar ( "number_isprime" , method , "method" , 2 )
  apifun_checkscalar ( "number_isprime" , imax , "imax" , 3 )
  apifun_checkscalar ( "number_isprime" , verbose , "verbose" , 4 )
  //
  // Check content
  // TODO : use apifun_checkflint
  apifun_checkflint ( "number_isprime" , n , "n" , 1 )
  apifun_checkrange ( "number_isprime" , n , "n" , 1 , -2^53 , 2^53 )
  apifun_checkoption ( "number_isprime" , method , "method" , 2 , ["modulo" "6k" "30k" "210k" "primes" "massdivide"] )
  apifun_checkrange ( "number_isprime" , imax , "imax" , 3 , 0 , 2^53 )
  //
  // Proceed...
  // The primality is not changed by the sign
  if ( n < 0 ) then
    n = -n
  end
  //
  select method
  case "modulo"
    [ isprime , status ] = isprime_modulo ( n , imax , verbose )
  case "6k"
    [ isprime , status ] = isprime_6k ( n , imax , verbose )
  case "30k"
    [ isprime , status ] = isprime_30k ( n , imax , verbose )
  case "210k"
    [ isprime , status ] = isprime_210k ( n , imax , verbose )
  case "primes"
    [ isprime , status ] = isprime_primes ( n , verbose )
  case "massdivide"
    [ isprime , status ] = isprime_massdivide ( n , verbose )
  else
    errmsg = sprintf ( gettext ( "%s: Unknown method %s\n" ) , "number_isprime" , method )
    error(errmsg)
  end
  if ( ~status & ( lhs == 1 ) ) then
    localstr = gettext("%s: The algorithm failed. Try to increase the value of the imax option.")
    error(msprintf(localstr, "number_isprime" ));
  end
  select lhs
  case 1 then
    varargout ( 1 ) = isprime
  case 2 then
    varargout ( 1 ) = isprime
    varargout ( 2 ) = status
  end
endfunction
//
// isprime_modulo --
//   Returns %t if the given number is prime.
//   Uses the modulo function.
//
function [ isprime , status ] = isprime_modulo ( n , imax , verbose )
    if ( n==1 ) then
      status = %t
      isprime = %f
      return
    end
    if ( n==2 ) then
      status = %t
      isprime = %t
      return
    end
    if ( modulo ( n , 2 ) ==0 ) then
      status = %t
      isprime = %f
      return
    end
    s = sqrt(n)
    d = 3
    for i = 1 : imax
      if ( verbose ) then
        number_isprimelog ( verbose , sprintf ( "Loop #%d/%d\n" , i , imax ) )
        number_isprimelog ( verbose , sprintf ( "Testing divisor d=%s\n" , string(d) ) )
      end
      if ( d > s ) then
        number_isprimelog ( verbose , sprintf ( "All integers to test are greater than s=%s, n=%s is prime, done.\n", string(s) , string(n) ) )
        status = %t
        isprime = %t
        return
      end
      if ( modulo ( n , d ) ==0 ) then
        number_isprimelog ( verbose , sprintf ( "Number d=%s is a divisor of n=%s, n is not a prime, done.\n" , string(d) , string(n)) )
        status = %t
        isprime = %f
        return
      end
      d = d + 2
    end
    // We cannot say if the number is prime or not.
    status = %f
    isprime = %f
endfunction
//
// isprime_6k --
//   Returns %t if the given number is prime.
//   Based on modulo function and on the fact that prime
//   numbers are of the form 6k + 1, 6k - 1
// Notes
//   The sequence of divisors to test is :
//   5 7
//   11 13
//   17 19
//   etc...
//
function [ isprime , status ] = isprime_6k ( n , imax , verbose )
    if ( n==1 ) then
      status = %t
      isprime = %f
      return
    end
    if ( or ( n == [ 2 3 ] ) ) then
      status = %t
      isprime = %t
      return
    end
    if ( or ( modulo ( n , [ 2 3 ] ) == 0 ) ) then
      status = %t
      isprime = %f
      return
    end
    s = sqrt(n)
    number_isprimelog ( verbose , sprintf ( "Sqrt(n)=%s\n" , string(s) ) )
    for i = 1 : imax
      d = 6 * i + [-1 1]
      if ( verbose ) then
        number_isprimelog ( verbose , sprintf ( "Loop #%d/%d\n" , i , imax ) )
        number_isprimelog ( verbose , sprintf ( "Testing against divisors d=[%s]\n" , strcat ( string ( d ) , " " ) ) )
      end
      if ( and ( d > s ) ) then
        number_isprimelog ( verbose , sprintf ( "All integers to test are greater than s=%s, n=%s is a prime, done.\n", string(s) , string(n) ) )
        status = %t
        isprime = %t
        return
      end
      if ( or ( d > s ) ) then
        // Some integers are greater than s, but not all.
        // We must still check the integers which are lower or equal than s.
        number_isprimelog ( verbose , sprintf ( "Some integers to test are lower than s=%s, reducing the set of divisors.\n" , string(s) ) )
        indices = find ( d <= s )
        d = d ( indices )
        number_isprimelog ( verbose , sprintf ( "Reduced set is i=[%s]\n" , strcat ( string ( d ) , " " ) ) )
      end
      if ( or ( modulo ( n , d ) == 0 ) ) then
        number_isprimelog ( verbose , sprintf ( "Number n=%s is a multiple of some d=[%s], not prime, done.\n" , string(n) , strcat ( string ( d ) , " " ) ) )
        status = %t
        isprime = %f
        return
      end
   end
   // We cannot say if the number is prime or not.
   status = %f
   isprime = %f
endfunction
//
// isprime_primes --
//   Returns %t if the given number is prime.
//   Based on primes function
//
function [ isprime , status ] = isprime_primes ( n , verbose )
  pr = number_primes ( n )
  isprime = ( pr ( $ ) == n )
  status = %t
endfunction
//
// isprime_30k --
//   Returns %t if the given number is prime.
//   Based on modulo function and on the fact that prime
//   numbers are of the form 30k + i, with i = 1, 7, 11, 13, 17, 19, 23, 29.
//   c#(5) = 2*3*5 = 30
//   The numbers 1,7,11,...,29 are the ones so that gcd(i,30)=1
//   We must first test for n = 2, 3 or 5.
//
function [ isprime , status ] = isprime_30k ( n , imax , verbose )
    if ( n==1 ) then
      status = %t
      isprime = %f
      return
    end
    if ( or ( n==[2 3 5] ) ) then
      status = %t
      isprime = %t
      return
    end
    if ( or ( modulo ( n , [ 2 3 5 ] ) ==0 ) ) then
      status = %t
      isprime = %f
      return
    end
    s = sqrt(n)
    number_isprimelog ( verbose , sprintf ( "Sqrt(n)=%s\n" , string(s) ) )
    for i = 1 : imax
      d = 30 * i + [1 7 11 13 17 19 23 29]
      if ( verbose ) then
        number_isprimelog ( verbose , sprintf ( "Loop #%d/%d\n" , i , imax ) )
        number_isprimelog ( verbose , sprintf ( "Testing against divisors d=[%s]\n" , strcat ( string ( d ) , " " ) ) )
      end
      if ( and ( d > s ) ) then
        number_isprimelog ( verbose , sprintf ( "All integers to test are greater than s=%s, done.\n", string(s) ) )
        status = %t
        isprime = %t
        return
      end
      if ( or ( d > s ) ) then
        // Some integers are greater than s, but not all.
        // We must still check the integers which are lower or equal than s.
        number_isprimelog ( verbose , sprintf ( "Some integers to test are lower than s=%s, reducing the set of divisors.\n" , string(s) ) )
        indices = find ( d <= s )
        d = d ( indices )
        number_isprimelog ( verbose , sprintf ( "Reduced set is i=[%s]\n" , strcat ( string ( d ) , " " ) ) )
      end
      if ( or ( modulo ( n , d ) == 0 ) ) then
        number_isprimelog ( verbose , sprintf ( "Number n=%s is a multiple of some d=[%s], not prime, done.\n" , string(n) , strcat ( string ( d ) , " " ) ) )
        status = %t
        isprime = %f
        return
      end
    end
    // We cannot say if the number is prime or not.
    status = %f
    isprime = %f
endfunction
//
// isprime_massdivide --
//   Returns %t if the given number is prime.
//   Based on a massive division of n by all odd numbers up to sqrt(n).
//   This is fast, but requires all lot of memory if the number is large.
//
function [ isprime , status ] = isprime_massdivide ( n , imax , verbose )
    if ( n==1 ) then
      status = %t
      isprime = %f
      return
    end
    if ( or ( n==[2 3 5] ) ) then
      status = %t
      isprime = %t
      return
    end
    if ( or ( modulo ( n , 2 ) == 0 ) ) then
      status = %t
      isprime = %f
      return
    end
    s = sqrt(n)
    number_isprimelog ( verbose , sprintf ( "Sqrt(n)=%s\n" , string(s) ) )
    if ( s < 3 ) then
      status = %t
      isprime = %t
      return
    end
    number_isprimelog ( verbose , sprintf ( "Dividing from 3 up to %s, by step 2\n" , string(s) ) )
    q = n ./ ( 3 : 2 : s );
    isprime = and ( q <> int (q) );
    status = %t
endfunction

//
// isprime_210k --
//   Returns %t if the given number is prime.
//   Based on modulo function and on the fact that prime
//   numbers are of the form 210k + i, with i = 1 11 13 17 ... 179 181 187 191 193 197 199 209
//   We have c#(7) = 210.
//   The numbers 1,11,...,209  are the ones so that gcd(i,210)=1
//   We must first test for n = 2, 3 or 5 or 7 .
//
function [ isprime , status ] = isprime_210k ( n , imax , verbose )
    if ( n==1 ) then
      status = %t
      isprime = %f
      return
    end
    if ( or ( n==[2 3 5 7 ] ) ) then
      status = %t
      isprime = %t
      return
    end
    if ( or ( modulo ( n , [ 2 3 5 7 ] ) ==0 ) ) then
      status = %t
      isprime = %f
      return
    end
    s = sqrt(n)
    number_isprimelog ( verbose , sprintf ( "Sqrt(n)=%s\n" , string(s) ) )
    table = [1 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 101 103 107 109 113 121 127 131 137 139 143 149 151 157 163 167 169 173 179 181 187 191 193 197 199 209 ]
    for i = 1 : imax
      d = 210 * i + table
      if ( verbose ) then
        number_isprimelog ( verbose , sprintf ( "Loop #%d/%d\n" , i , imax ) )
        number_isprimelog ( verbose , sprintf ( "Testing against divisors d=[%s]\n" , strcat ( string ( d ) , " " ) ) )
      end
      if ( and ( d > s ) ) then
        number_isprimelog ( verbose , sprintf ( "All integers to test are greater than s=%s, done.\n", string(s) ) )
        status = %t
        isprime = %t
        return
      end
      if ( or ( d > s ) ) then
        // Some integers are greater than s, but not all.
        // We must still check the integers which are lower or equal than s.
        number_isprimelog ( verbose , sprintf ( "Some integers to test are lower than s=%s, reducing the set of divisors.\n" , string(s) ) )
        indices = find ( d <= s )
        d = d ( indices )
        number_isprimelog ( verbose , sprintf ( "Reduced set is i=[%s]\n" , strcat ( string ( d ) , " " ) ) )
      end
      if ( or ( modulo ( n , d ) == 0 ) ) then
        number_isprimelog ( verbose , sprintf ( "Number n=%s is a multiple of some d=[%s], not prime, done.\n" , string(n) , strcat ( string ( d ) , " " ) ) )
        status = %t
        isprime = %f
        return
      end
    end
    // We cannot say if the number is prime or not.
    status = %f
    isprime = %f
endfunction

function number_isprimelog ( verbose , msg )
  if ( verbose ) then
    mprintf ( "%s\n" , msg )
  end
endfunction

