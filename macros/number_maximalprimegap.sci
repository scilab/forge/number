// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [gap,p] = number_maximalprimegap ( n )
  // Returns the maximal prime gap.
  //
  // Calling Sequence
  //   [gap,p] = number_maximalprimegap ( n )
  //
  // Parameters
  //  n : a 1-by-1 matrix of floating point integers, the largest prime to consider
  //  gap : a m-by-1 matrix of floating point integers, the difference of prime numbers
  //  p : a m-by-1 matrix of floating point integers, the corresponding prime number
  //
  // Description
  //   Returns the maximal prime gap function.
  //   For k=1,2,...,m, we have gap(k) == q-p(k), where q is the smallest prime greater than
  //   p(k).
  //   On output, the integer p(k) is the smallest prime number so that the difference
  //   of primes is gap(k).
  //
  // Examples
  // [gap,p] = number_maximalprimegap ( 1500 )
  // expected_gap = [1 2 4 6 8 14 18 20 22 34]'
  // expected_p = [2 3 7 23 89 113 523 887 1129 1327]'
  //
  // // Plot it
  // stacksize("max");
  // scf();
  // n=1000000;
  // [gap,p] = number_maximalprimegap ( n );
  // plot(gap,log(p)^2,"bo")
  // plot(gap,gap,"r-")
  // legend(["Empirical","Cramer-Shanks Conjecture"]);
  //
  // Bibliography
  // http://mathworld.wolfram.com/PrimeGaps.html
  //
  // Authors
  // Copyright (C) 2010 - Michael Baudin
  //

  [lhs,rhs]=argn();
  apifun_checkrhs ( "number_maximalprimegap" , rhs , 1 )
  apifun_checklhs ( "number_maximalprimegap" , lhs , 0:2 )
  //
  // Check arguments
  //
  // Check type
  apifun_checktype ( "number_maximalprimegap" , n , "n" , 1 , "constant" )
  //
  // Check size
  // Nothing to check
  //
  // Check content
  // TODO : use apifun_checkflint
  apifun_checkflint ( "number_maximalprimegap" , n , "n" , 1 )
  apifun_checkrange ( "number_maximalprimegap" , n , "n" , 1 , -2^53 , 2^53 )
  //
  // Proceed...
  allprimes = number_primes(n)
  d = diff(allprimes)
  p(1) = allprimes(1)
  gap(1) = d(1)
  k = 2
  for i = 1 : size(d,"*")
    if ( d(i) > gap(k-1) ) then
      p(k)=allprimes(i)
      gap(k) = d(i)
      k = k+1
    end
  end
endfunction


