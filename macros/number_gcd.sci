// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
// Copyright (C) INRIA - Farid BELAHCENE
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function result = number_gcd ( varargin )
  // Greatest common divisor.
  //
  // Calling Sequence
  //   result = number_gcd ( m , n )
  //   result = number_gcd ( m , n , method )
  //
  // Parameters
  // m : a 1x1 matrix of floating point integers
  // n : a 1x1 matrix of floating point integers
  // method : a 1x1 matrix of strings, the algorithm to use (default method=euclidian)
  // result : a 1x1 matrix of floating point integers, the least common multiple
  //
  // Description
  // Returns the greatest common divisor of m and n.
  //
  // The following values of method are availble.
  // <variablelist>
  // <varlistentry>
  //   <term> method="euclidian" </term>
  //    <listitem>
  //    <para>
  //    Uses a naive Euclid's recursive algorithm.
  //    </para>
  //    </listitem>
  //  </varlistentry>
  //  <varlistentry>
  //    <term> method="brute" </term>
  //    <listitem>
  //    <para>
  //    Uses a brute-force method, based on systematic search
  //    for divisors.
  //    </para>
  //    </listitem>
  //  </varlistentry>
  //  <varlistentry>
  //    <term> method="binary" </term>
  //    <listitem>
  //    <para>
  //    Uses a naive binary recursive method.
  //    This is similar to Algorithm B in section 4.5.2 of TAO.
  //    </para>
  //    </listitem>
  //  </varlistentry>
  //  <varlistentry>
  //  <term> method="euclidianiterative" </term>
  //    <listitem>
  //    <para>
  //    Uses Euclid's algorithm with a loop (instead of recursive).
  //    </para>
  //    </listitem>
  //  </varlistentry>
  //  <varlistentry>
  //    <term> method="binaryiterative" </term>
  //    <listitem>
  //    <para>
  //    Uses binary algorithm with a loop (instead of recursive).
  //    </para>
  //    </listitem>
  //  </varlistentry>
  //  </variablelist>
  //
  // Examples
  // number_gcd ( 84 , 18 ) // 6
  // number_gcd ( 18 , 84 ) // 6
  //
  // // With negative integers
  // computed = number_gcd ( 18 , -84 ) // 6
  //
  // // Test all algos
  // algos = [
  //   "euclidian"
  //   "brute"
  //   "binary"
  //   "euclidianiterative"
  //   "binaryiterative"
  // ];
  // mprintf("  %s\n", strcat(algos," "));
  // for m = 1 : 20
  // for n = 1 : 20
  //   msg="";
  //   msg=msg+msprintf("gcd(%d,%d) = ",m, n);
  //   for method = algos'
  //     d = number_gcd (m,n,method);
  //     msg=msg+msprintf("%s ",string(d));
  //   end
  //   msg=msg+msprintf("\n");
  //   mprintf("%s\n",msg);
  // end
  // end
  //
  // Bibliography
  // "The Art of Computer Programming", Volume 2, Seminumerical Algorithms, Donald Knuth, Addison-Wesley
  //
  // Authors
  // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
  // Copyright (C) INRIA - Farid BELAHCENE
  //

    //
    // Load Internals lib
    path = get_function_path("number_gcd")
    path = fullpath(fullfile(fileparts(path)))
    numberinternalslib = lib(fullfile(path,"internals"));

  [lhs, rhs] = argn()
  apifun_checkrhs ( "number_gcd" , rhs , 2:3 )
  apifun_checklhs ( "number_gcd" , lhs , 0:1 )
  //
  // Get arguments
  m = varargin ( 1 )
  n = varargin ( 2 )
  method = apifun_argindefault (  varargin , 3 , "euclidian" )
  //
  // Check type
  apifun_checktype ( "number_gcd" , m , "m" , 1 , "constant" )
  apifun_checktype ( "number_gcd" , n , "n" , 2 , "constant" )
  apifun_checktype ( "number_gcd" , method , "method" , 3 , "string" )
  //
  // Check size
  apifun_checkscalar ( "number_gcd" , m , "m" , 1 )
  apifun_checkscalar ( "number_gcd" , n , "n" , 2 )
  apifun_checkscalar ( "number_gcd" , method , "method" , 3 )
  //
  // Check content
  apifun_checkflint ( "number_gcd" , m , "m" , 1 )
  apifun_checkrange ( "number_gcd" , m , "m" , 1 , -2^53 , 2^53 )
  apifun_checkflint ( "number_gcd" , n , "n" , 2 )
  apifun_checkrange ( "number_gcd" , n , "n" , 2 , -2^53 , 2^53 )
  apifun_checkoption ( "number_gcd" , method , "method" , 3 , ["euclidian" "brute" "binary" "euclidianiterative" "binaryiterative"] )
  //
  // Proceed...
  // gcd ( a , b ) = gcd ( -a , b ) = gcd ( a , -b)
  // therefore, take into account only for positive values.
  if ( m < 0 ) then
    m = -m
  end
  if ( n < 0 ) then
    n = -n
  end
  select method
  case "euclidian"
    result = number_gcdeuclidint ( m , n )
  case "brute"
    result = gcd_brute ( m , n )
  case "binary"
    result = gcd_binary ( m , n )
  case "euclidianiterative"
    result = gcd_euclidianiterative ( m , n )
  case "binaryiterative"
    result = gcd_binaryiterative ( m , n )
  else
    errmsg = sprintf ( gettext ( "%s: Unknown method %s" ) , "number_gcd" , method )
    error(errmsg)
  end
endfunction
//
// gcd_brute --
//   Returns the greatest common divisor of m and n
//   Use a brute-force method.
// Arguments:
//   m, n : the positive integer numbers
//
function result = gcd_brute ( m , n )
    if ( m > n ) then
      c = n
    else
      c = m
    end
    while ( %t )
      r1 = modulo ( m , c )
      r2 = modulo ( n , c )
      if ( ( r1==0 ) & ( r2==0 ) ) then
        break
      end
      c = c - 1
    end
    result = c
endfunction
//
// gcd_binary --
//   Returns the greatest common divisor of m and n
//   Use a naive binary method.
// Arguments:
//   m, n : the positive integer numbers
//
function result = gcd_binary ( m , n )
    if ( m==0 ) then
       result = n
       return
    end
    if ( n==0 ) then
       result = m
       return
    end
    meven = ( modulo ( m , 2 ) == 0 )
    neven = ( modulo ( n , 2 ) == 0 )
    if ( meven ) then
        if ( neven ) then
        // m even, n even
          m2 = m/2
          n2 = n/2
          d = gcd_binary ( m2 , n2 )
          result = 2 * d
          return
        else
        // m even, n odd
          m2 = m/2
          result = gcd_binary ( m2 , n )
          return
        end
     else
        if ( neven ) then
        // m odd, n even
          n2 = n/2
          result = gcd_binary ( m , n2 )
          return
         else
        // m odd, n odd
          if ( m<=n ) then
            d = (n-m)/2
            result = gcd_binary ( m , d )
            return
          else
            d = (m-n)/2
            result = gcd_binary ( d , n )
            return
          end
        end
    end
endfunction
//
// gcd_euclidianiterative --
//   Returns the greatest common divisor of m and n
//   Use a naive, loop-based, Euclidian method.
// Arguments:
//   m, n : the positive integer numbers
//
  function result = gcd_euclidianiterative ( m , n )
    u = m
    v = n
    while ( u > 0 & v > 0 )
      if ( u >= v ) then
        u = modulo ( u , v )
      else
        v = modulo ( v , u )
      end
    end
    if ( u==0 ) then
      result = v
    elseif ( v==0 ) then
      result  = u
    else
      error(msprintf(gettext("%s: Unexpected case u = %d, v = %d"),"gcd_euclidianiterative" , u , v ));
    end
endfunction
//
// gcd_binaryiterative --
//   Returns the greatest common divisor of m and n
//   Use a naive, loop-based, binary method.
// Arguments:
//   m, n : the positive integer numbers
//
  function result = gcd_binaryiterative ( m , n )
    u = m
    v = n
  // functioness until u and v are odd,
  // or u or v is zero
    d = 1
    while ( u>0 & v>0 )
      ueven = ( modulo ( u , 2 ) ==0 )
      veven = ( modulo ( v , 2 ) ==0 )
      if ( ueven ) then
        if ( veven ) then
        // u even, v even
          u = u/2
          v = v/2
          d = 2 * d
         else
        // u even, v odd
          u = u/2
         end
      else
        if ( veven ) then
        // u odd, v even
          v = v/2
        else
        // u odd, v odd
          if ( u<=v ) then
            v = (v-u)/2
          else
            u = (u-v)/2
          end
        end
      end
    end
    if ( u==0 ) then
      result = d * v
    elseif ( v==0 ) then
      result = d * u
    else
      error(msprintf(gettext("%s: Unexpected case u = %d, v = %d"),"gcd_binaryiterative" , u , v ));
    end
endfunction
