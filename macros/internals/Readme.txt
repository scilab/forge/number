Michael Baudin, May 2012
Note on the purpose of the internal functions.

The "internal" functions in this folder are because of a 
limitation in the apifun module.
Indeed, using apifun can ruin performance :

http://forge.scilab.org/index.php/p/apifun/issues/727/

The problem is that 
 * number_probableprime is calling number_powermod within 
   a loop,
 * number_powermod is calling number_asmodm within 
   a loop.
But all of them use apifun to check their args.
All in all, 
 * the performance of number_asmodm is bad,
 * the performance of number_powermod is very bad,
 * the performance of number_probableprime is unacceptable.
This is why we created these internal functions, without 
argument checking. 
We make the (risky) assumption that the developper 
of the module knows for what types of arguments we 
can call the functions. 
When the perf. issue in apifun is fixed, the code can be reintegrated 
directly into the body of the main functions and the 
"internal" functions can be deleted.
Michael Baudin, May 2012
Note on the purpose of the internal functions.

The "internal" functions in this folder are because of a 
limitation in the apifun module.
Indeed, using apifun can ruin performance :

http://forge.scilab.org/index.php/p/apifun/issues/727/

The problem is that 
 * number_probableprime is calling number_powermod within 
   a loop,
 * number_powermod is calling number_asmodm within 
   a loop.
But all of them use apifun to check their args.
All in all, 
 * the performance of number_asmodm is bad,
 * the performance of number_powermod is very bad,
 * the performance of number_probableprime is unacceptable.
This is why we created these internal functions, without 
argument checking. 
We make the (risky) assumption that the developper 
of the module knows for what types of arguments we 
can call the functions. 
When the perf. issue in apifun is fixed, the code can be reintegrated 
directly into the body of the main functions and the 
"internal" functions can be deleted.
