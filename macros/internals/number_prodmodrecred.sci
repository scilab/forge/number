// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function p = number_prodmodrecred(a,s,m)
    // Returns a*s (modulo m).
    //
    // Calling Sequence
    //   p = number_prodmodrecred ( a , s , m )
    //
    // Parameters
    // a : a 1x1 matrix of floating point integers.
    // s : a 1x1 matrix of floating point integers
    // m : a 1x1 matrix of floating point integers, the modulo
    // p : a 1x1 matrix of floating point integers, the result of a*s modulo m.
    //
    // Description
    // Returns the result of a*s (modulo m) by recursive reduction.
    // This algorithm was adapted from L'Ecuyer and Cote (1991).
    //
    // The code makes the assumption that 0<a<m and 0<s<m
    // and does not check these assumptions.
    // These assumtions are more flexible than the assumptions
    // required by number_asmodmshrage.
    //
    // Note :
    // MB replaced the original loop
    // while (p<0)
    //     p = p + m
    // end
    // by :
    // p = pmodulo(p,m)
    // This is because the worst-case
    // is horrible in Scilab, because of the
    // terrible loop.
    //
    // Bibliography
    // Implementing a Random Number Package with Splitting Facilities, Pierre L'Ecuyer, Serge Cote, ACM TOMS, Vol. 17, No.1, March 1991, Pages 98-111
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin

    // Here, we use b=54, since all integers
    // in the range [-2^53,2^53] are well represented.
    b = 54;
    d = (b-2)/2;
    H = 2^d;

    if a > m then
        a = pmodulo(a, m);
    end
    if s > m then
        s = pmodulo(s, m);
    end
    if a == 0 | s == 0 then
        p = 0;
        return
    end

    q = int(m/a);
    r = m - a*q;
    k = int(s/q);
    t = s - k*q;
    p = a*t;
    sgn = -1;
    while a > H & r <> 0 & k <> 0
        a = r;
        s = k;
        q = int(m/a);
        r = m - a*q;
        k = int(s/q);
        t = s-k*q;
        p = p+sgn*a*t;
        sgn = -sgn;
        if sgn*p > 0 then
            p = p - sgn*m;
        end
    end
    if k <> 0 then
        p = p + sgn*k*r;
    end
    p = pmodulo(p, m);
endfunction
