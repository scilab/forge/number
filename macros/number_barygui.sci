// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function number_baryguih = number_barygui ( varargin )
  //   Plots a gui to see the digits of a number.
  //
  // Calling Sequence
  //   h = number_barygui ( n )
  //   h = number_barygui ( n , b )
  //
  // Parameters
  //   n : a 1x1 matrix of floating point integers, must be positive, the integer to decompose
  //   b : a 1x1 matrix of floating point integers, must be positive, the basis
  //   h : a handle to the graphics
  //
  // Description
  // Displays a gui to see digits of n in base basis.
  // This gui is expected to be unique (it is not currently possible
  // to display two bary GUIs).
  //
  // Uses the dispmap module to display the digits. 
  //
  // Examples
  // h = number_barygui ( 4 );
  // number_baryguiclose();
  //
  // h = number_barygui ( 4 , 3 );
  // number_baryguiclose();
  //
  // Authors
  // Copyright (C) 2010 - Michael Baudin
  //

  [lhs,rhs]=argn();
  apifun_checkrhs ( "number_barygui" , rhs , 1:2 )
  apifun_checklhs ( "number_barygui" , lhs , 0:1 )
  //
  // Get arguments
  n = varargin ( 1 )
  basis = apifun_argindefault (  varargin , 2 , 2 )
  //
  // Check type
  apifun_checktype ( "number_barygui" , n , "n" , 1 , "constant" )
  apifun_checktype ( "number_barygui" , basis , "basis" , 2 , "constant" )
  //
  // Check size
  apifun_checkscalar ( "number_barygui" , n , "n" , 1 )
  apifun_checkscalar ( "number_barygui" , basis , "basis" , 2 )
  //
  // Check content
  // TODO : use apifun_checkflint
  apifun_checkflint ( "number_barygui" , n , "n" , 1 )
  apifun_checkrange ( "number_barygui" , n , "n" , 1 , 0 , 2^53 )
  apifun_checkflint ( "number_barygui" , basis , "basis" , 2 )
  apifun_checkrange ( "number_barygui" , basis , "basis" , 2 , 2 , 2^53 )

  //
  // Create and configure a global data structure to hold the settings
  global number_baryguih
  number_baryguih = tlist(["T_BARYGUI" 
    "digitsmax"
    "basis"
    "order"
    "n"
    "digitwidth"
    "digitheight"
    "matrixfig" 
    "controlfig" 
    "hclose"
    "hentry"
    "hbutton"
    "hlittlebigendian"
    "hnbbits"
    ]);
  number_baryguih.n = n
  number_baryguih.digitsmax = 16
  number_baryguih.basis = basis
  number_baryguih.order = "littleendian"
  number_baryguih.hlittlebigendian = 5;
  number_baryguih.digitwidth = 40
  number_baryguih.digitheight = 40
  // Display the digits
  number_tbgui_drawdigits (  )
  // Display the controls
  number_tbgui_uicontrols (  )
endfunction

// number_tbgui_drawdigits --
//   Opens a graphics with the digits of the current number.
function number_tbgui_drawdigits (  )
  global number_baryguih
  // Extract fields
  n = number_baryguih.n
  basis = number_baryguih.basis
  order = number_baryguih.order
  digitsmax = number_baryguih.digitsmax
  // Decompose into digits
  digits = number_tobary ( n , basis , order , digitsmax )'
  // Create the matrix plot
  digitsmax = number_baryguih.digitsmax
  figWidth  = number_baryguih.digitwidth * digitsmax
  figHeight  = number_baryguih.digitheight
  number_baryguih.matrixfig = dispmat_plotnew ( digits , figWidth , figHeight )
endfunction

// number_tbgui_uicontrols --
//   Creates a figure containing controls for the digits.
function number_tbgui_uicontrols (  )
  global number_baryguih
  // Create a figure for the controls
  figWidth  = 400;
  figHeight  = 240;
  fig=figure("Position",[10 200 figWidth figHeight]);
  number_baryguih.controlfig = fig;
  fig.auto_resize = "off";
  set(fig,"figure_name","Bary Controls");
  fontsize = 12;
  // Add controls : "Print" + "Close"
  yrow = 0
  herow = 30
  x = 0
  y = yrow
  wi = 100
  he = herow
  uicontrol ( fig , ...
      "style" , "pushbutton" , ...
      "position" , [x y wi he] , ...
      "Horizontalalignment","left",...
      "FontSize", fontsize, ...
      "String","Print" , ...
      "Callback" , "number_tbgui_print()" );
  x = 100
  y = yrow
  wi = 100
  he = herow
  number_baryguih.hclose = uicontrol ( fig , ...
      "style" , "pushbutton" , ...
      "position" , [x y wi he] , ...
      "Horizontalalignment","left",...
      "FontSize", fontsize, ...
      "String","Close" , ...
      "Callback" , "number_baryguiclose()" );
  // Add controls : "+1", "-1", "*2", "/2"
  yrow = yrow + herow
  herow = 30
  x = 0
  y = yrow
  wi = 100
  he = 30
  uicontrol ( fig , ...
      "style" , "pushbutton" , ...
      "position" , [x y wi he] , ...
      "Horizontalalignment","left",...
      "FontSize", fontsize, ...
      "String","+1" , ...
      "Callback" , "number_tbguiplus(1)" );
  x = 100
  y = yrow
  wi = 100
  he = 30
  uicontrol ( fig , ...
      "style" , "pushbutton" , ...
      "position" , [x y wi he] , ...
      "Horizontalalignment","left",...
      "FontSize", fontsize, ...
      "String","-1" , ...
      "Callback" , "number_tbguiplus(-1)" );
  x = 200
  y = yrow
  wi = 100
  he = 30
  uicontrol ( fig , ...
      "style" , "pushbutton" , ...
      "position" , [x y wi he] , ...
      "Horizontalalignment","left",...
      "FontSize", fontsize, ...
      "String","*2" , ...
      "Callback" , "number_tbguitimes(2)" );
  x = 300
  y = yrow
  wi = 100
  he = 30
  uicontrol ( fig , ...
      "style" , "pushbutton" , ...
      "position" , [x y wi he] , ...
      "Horizontalalignment","left",...
      "FontSize", fontsize, ...
      "String","/2" , ...
      "Callback" , "number_tbguitimes(0.5)" );
  // Add controls : "Decimal:" + entry + "Convert To Binary"
  yrow = yrow + herow
  herow = 30
  x = 0
  y = yrow
  wi = 100
  he = herow
  uicontrol ( fig , ...
      "style" , "text" , ...
      "position" , [x y wi he] , ...
      "Horizontalalignment","left",...
      "FontSize", fontsize, ...
      "String","Decimal:" );
  x = 100
  y = yrow
  wi = 100
  he = herow
  number_baryguih.hentry = uicontrol ( fig , ...
      "style" , "edit" , ...
      "position" , [x y wi he] , ...
      "Horizontalalignment","left",...
      "FontSize", fontsize, ...
      "String",string(n) );
  x = 200
  y = yrow
  wi = 200
  he = herow
  number_baryguih.hbutton = uicontrol ( fig , ...
      "style" , "pushbutton" , ...
      "position" , [x y wi he] , ...
      "Horizontalalignment","left",...
      "FontSize", fontsize, ...
      "String","Convert To B-ary" , ...
      "Callback" , "number_tbgui_convertobin()" );
  // Add controls : LittleEndian/ BigEndian + 1,2,4,8,16,32,64 bits
  yrow = yrow + herow
  herow = 150
  x = 0
  y = yrow
  wi = 200
  he = herow
  VAR = uicontrol ( fig , ...
      "style" , "listbox" , ...
      "position" , [x y wi he] , ...
      "FontSize", fontsize, ...
      "String", "LittleEndian|BigEndian");//, ...
  number_baryguih.hlittlebigendian = VAR;
  //    "Callback" , "number_tbgui_littlebig()" );
    disp("LBL 1");
    disp(VAR, "VAR");
    disp(number_baryguih.hlittlebigendian);
  set(number_baryguih.hlittlebigendian, "Value", 1);
    disp("LBL 2");
  x = 200
  y = yrow
  wi = 200
  he = herow
  number_baryguih.hnbbits = uicontrol ( fig , ...
      "style" , "listbox" , ...
      "position" , [x y wi he] , ...
      "FontSize", fontsize, ...
      "String", "1 digit|2 digits|4 digits|8 digits|16 digits|32 digits|64 digits", ...
      "Callback" , "number_tbguinbdig()" );
  set(number_baryguih.hnbbits, "Value", 5);
endfunction

// number_tbgui_convertobin --
//   Callback associated with the convert to binary button.
function number_tbgui_convertobin (  )
  global number_baryguih
  // Get the integer to convert
  s = "n = " + number_baryguih.hentry.String
  execstr(s)
  number_baryguih.n = n
  // Update the matrix of digits
  number_tbguiupdatedig (  )
endfunction

// number_tbgui_littlebig --
//   Callback associated with the "little/big endian" listbox.
function number_tbgui_littlebig (  )
  global number_baryguih
  select number_baryguih.hlittlebigendian.Value
  case 1
    order = "littleendian"
  case 2
    order = "bigendian"
  else
    errmsg = msprintf(gettext("%s: Unexpected order: %d."), "number_tbgui_littlebig", number_baryguih.hlittlebigendian.Value);
    error(errmsg)
  end
  number_baryguih.order = order
  // Update the matrix of digits
  number_tbguiupdatedig (  )
endfunction

// number_tbguinbdig --
//   Callback associated with the "1,2,4,8,16,32,64" listbox.
function number_tbguinbdig (  )
  global number_baryguih
  select number_baryguih.hnbbits.Value
  case 1
    nbits = 1
  case 2
    nbits = 2
  case 3
    nbits = 4
  case 4
    nbits = 8
  case 5
    nbits = 16
  case 6
    nbits = 32
  case 7
    nbits = 64
  else
    errmsg = msprintf(gettext("%s: Unexpected number of bits: %d."), "number_tbguinbdig", number_baryguih.hnbbits.Value);
    error(errmsg)
  end
  // Extract fields
  n = number_baryguih.n
  basis = number_baryguih.basis
  order = number_baryguih.order
  // Decompose into digits, to make sure that it is possible,
  // before actually redrawing the graphics
  digits = number_tobary ( n , basis , order , nbits )'
  // We are here, no error was generated : continue !
  number_baryguih.digitsmax = nbits
  // Re-draw the matrix of digits
  // This is necessary, because the number of digits has changed:
  // we cannot just re-configure the existing digits.
  number_tbguiredrawdig (  )
endfunction

// number_tbguiredrawdig --
//   Re-draw the digits graphics
function number_tbguiredrawdig (  )
  global number_baryguih
  drawlater()
  close ( number_baryguih.matrixfig.figure )
  number_tbgui_drawdigits (  )  
  drawnow()
endfunction

// number_tbguiupdatedig --
//   Update the digits in the digits graphics.
//   (but do not close the window, open a new one).
function number_tbguiupdatedig (  )
  global number_baryguih
  drawlater()
  // Extract fields
  n = number_baryguih.n
  basis = number_baryguih.basis
  order = number_baryguih.order
  digitsmax = number_baryguih.digitsmax
  // Decompose into digits
  digits = number_tobary ( n , basis , order , digitsmax )'
  // Configure the matrix plot
  m = number_baryguih.matrixfig
  for i = 1 : digitsmax
    uientry = dispmat_plotgetcell ( m , 1 , i )
    uientry.String = string(digits(i))
  end
  drawnow()
endfunction

// number_tbguiplus --
//   Try to add s to the number, if possible.
function number_tbguiplus ( s )
  global number_baryguih
  // Extract fields
  n = number_baryguih.n
  basis = number_baryguih.basis
  order = number_baryguih.order
  digitsmax = number_baryguih.digitsmax
  // Update number
  n = n + s
  // Compute digits
  digits = number_tobary ( n , basis , order , digitsmax )'
  // Update the gui
  number_baryguih.n = n
  number_baryguih.hentry.String = string(n)
  number_tbguiupdatedig (  )
endfunction



// number_tbguitimes --
//   Try to multiply the number by s, if possible.
function number_tbguitimes ( s )
  global number_baryguih
  // Extract fields
  n = number_baryguih.n
  basis = number_baryguih.basis
  order = number_baryguih.order
  digitsmax = number_baryguih.digitsmax
  // Update number
  n = int(n * s)
  // Compute digits
  digits = number_tobary ( n , basis , order , digitsmax )'
  // Update the gui
  number_baryguih.n = n
  number_baryguih.hentry.String = string(n)
  number_tbguiupdatedig (  )
endfunction

// number_tbgui_print --
//   Prints the digits in the console
function number_tbgui_print ( )
  global number_baryguih
  // Extract fields
  n = number_baryguih.n
  basis = number_baryguih.basis
  order = number_baryguih.order
  digitsmax = number_baryguih.digitsmax
  // Compute digits
  digits = number_tobary ( n , basis , order , digitsmax )'
  disp(digits)
endfunction
