// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function arecoprime = number_coprime ( m , n )
  // Checks if two numbers are relatively prime.
  // 
  // Calling Sequence
  //   arecoprime = number_coprime ( m , n )
  //
  // Parameters
  //   m : a 1x1 matrix of floating point integers, must be positive
  //   n : a 1x1 matrix of floating point integers, must be positive
  //   arecoprime : a 1x1 matrix of booleans
  //
  // Description
  //   Returns %t if m and n are relative primes. 
  //
  //   The two integers m and n are said to be coprime or relatively prime 
  //   if the only positive integer that evenly divides both of them is 1.
  //
  //   In other words, m and n are coprime if an only if
  //
  //<screen>
  //number_gcd(m,n)==1
  //</screen>
  //
  // Examples
  // computed = number_coprime ( 84 , 18 ) // %f 
  // computed = number_coprime ( 17 , 19 ) // %t
  //
  // Authors
  // Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
  // Copyright (C) 2012 - Michael Baudin
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "number_coprime" , rhs , 2 )
  apifun_checklhs ( "number_coprime" , lhs , 0:1 )
  //
  // Check type
  apifun_checktype ( "number_coprime" , m , "m" , 1 , "constant" )
  apifun_checktype ( "number_coprime" , n , "n" , 2 , "constant" )
  //
  // Check size
  apifun_checkscalar ( "number_coprime" , m , "m" , 1 )
  apifun_checkscalar ( "number_coprime" , n , "n" , 2 )
  //
  // Check content
  apifun_checkflint ( "number_coprime" , m , "m" , 1 )
  apifun_checkrange ( "number_coprime" , m , "m" , 1 , 0 , 2^53 )
  apifun_checkflint ( "number_coprime" , n , "n" , 2 )
  apifun_checkrange ( "number_coprime" , n , "n" , 2 , 0 , 2^53 )
  //
  d = number_gcd ( m , n )
  arecoprime = ( d == 1 )
endfunction

