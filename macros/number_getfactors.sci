// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function varargout = number_getfactors ( varargin )
    // Compute the divisors of a number.
    //
    // Calling Sequence
    //   fac = number_getfactors ( n )
    //   [ fac , status ] = number_getfactors ( n )
    //   [ fac , status ] = number_getfactors ( n , method )
    //   [ fac , status ] = number_getfactors ( n , method , imax )
    //   [ fac , status ] = number_getfactors ( n , method , imax , verbose )
    //   [ fac , status ] = number_getfactors ( n , method , imax , verbose , pollardseeds )
    //
    // Parameters
    //  n : a 1x1 matrix of floating point integers
    //  method : a 1x1 matrix of strings, the method to use (default "fasttrialdivision"). Available are method = "fasttrialdivision", "trialdivision", "memorylesstd", "fermat", "pollard-rho".
    //  imax : a 1-by-1 matrix of floating point integers, the number of steps in the loop (default imax=10000)
    //  verbose : a 1-by-1 matrix of booleans, set to true to display messages (default verbose=%f)
    //  pollardseeds : a m-by-1 matrix of floating point integers, the seeds for Pollard's rho. Only for method="pollard-rho". Default pollardseeds = [-3 1 3 5].
    //  fac : a m-by-1 matrix of floating point integers, the divisors of n
    //  status : %t if the factorization is a sucecss, %f if the factorization is a failure
    //
    // Description
    //   Returns all the factors (i.e. divisors) of n.
    //   Each entry fac(i) is a divisor of n, i.e. is so that
    //
    //<screen>
    //n/fac(i)
    //</screen>
    //
    //has no fractional part, for i=1,2,...,m.
    //
    //  In case of success, all factors are returned in the result row matrix,
    //  and success = %t.
    //
    //  In case of failure, if there is one output argument, an error
    //  is generated.
    //  In case of failure, if there are two output arguments,
    //  success = %f and the factor which were found are returned in result.
    //  This feature allows to retrieve the factors which could be found (partial success),
    // even if, globally, the algorithm failed.
    //
    //  Any input argument equal to the empty matrix is replaced by its default value.
    //
    // For details on the methods, see number_factor.
    //
    // Examples
    // // Factors of 120 : 1 2 3 4 5 6 8 10 12 15 20 24 30 40 60 120
    // lst = number_getfactors(120)
    // // Factors of 1024 : 1 2 4 8 16 32 64 128 256 512 1024
    // lst = number_getfactors(1024)
    //
    // Bibliography
    // http://rosettacode.org/wiki/Factors_of_an_integer#Prime_factoring
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    //

    [lhs,rhs]=argn();
    apifun_checkrhs ( "number_getfactors" , rhs , 1:5 )
    apifun_checklhs ( "number_getfactors" , lhs , 1:2 )
    //
    // Get arguments
    n = varargin ( 1 )
    method = apifun_argindefault (  varargin , 2 , "fasttrialdivision" )
    imax = apifun_argindefault (  varargin , 3 , 10000 )
    verbose = apifun_argindefault (  varargin , 4 , %f )
    pollardseeds = apifun_argindefault (  varargin , 5 , [-3 1 3 5] )
    //
    // Check arguments
    //
    // Check type
    apifun_checktype ( "number_getfactors" , n , "n" , 1 , "constant" )
    apifun_checktype ( "number_getfactors" , method , "method" , 2 , "string" )
    apifun_checktype ( "number_getfactors" , imax , "imax" , 3 , "constant" )
    apifun_checktype ( "number_getfactors" , verbose , "verbose" , 4 , "boolean" )
    apifun_checktype ( "number_getfactors" , pollardseeds , "pollardseeds" , 5 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "number_getfactors" , n , "n" , 1 )
    apifun_checkscalar ( "number_getfactors" , method , "method" , 2 )
    apifun_checkscalar ( "number_getfactors" , imax , "imax" , 3 )
    apifun_checkscalar ( "number_getfactors" , verbose , "verbose" , 4 )
    apifun_checkvector ( "number_getfactors" , pollardseeds , "pollardseeds" , 5 , size(pollardseeds,"*") )
    //
    // Check content
    apifun_checkflint ( "number_getfactors" , n , "n" , 1 )
    apifun_checkrange ( "number_getfactors" , n , "n" , 1 , -2^53 , 2^53 )
    apifun_checkoption ( "number_getfactors" , method , "method" , 2 , ["fasttrialdivision","trialdivision","memorylesstd","fermat","pollard-rho"] )
    apifun_checkrange ( "number_getfactors" , imax , "imax" , 3 , 0 , 2^53 )
    apifun_checkflint ( "number_getfactors" , pollardseeds , "pollardseeds" , 5 )
    apifun_checkrange ( "number_getfactors" , pollardseeds , "pollardseeds" , 5 , -2^53 , 2^53 )
    //
    // Proceed...
    [ p , e , status ] = number_getprimefactors ( n , method , imax , verbose , pollardseeds )
    if (~status) then
        localstr = gettext("%s: The algorithm failed. Try to increase the value of the imax option.")
        select lhs
        case 1 then
            error(msprintf(localstr, "number_getfactors" ));
        case 2 then
            varargout ( 1 ) = []
            varargout ( 2 ) = status
        end
        return
    end
    //
    n_f = size(p,"*")
    fac = []
    len2 = 1
    fac(1)=1
    for i = 1:n_f
        q = p(i)
        for j = 1:e(i)
            for k = 1:len2
                fac($+1) = fac(k) * q
            end
            q = q * p(i)
        end
        len2 = size(fac,"*")
    end
    fac = gsort(fac,"g","i")
    select lhs
    case 1 then
        varargout ( 1 ) = fac
    case 2 then
        varargout ( 1 ) = fac
        varargout ( 2 ) = status
    end
endfunction
