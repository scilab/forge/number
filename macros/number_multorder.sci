// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function k = number_multorder ( a , m )
    // Computes the multiplicative order modulo m.
    //
    // Calling Sequence
    //   k = number_multorder ( a , m )
    //
    // Parameters
    // a : a 1x1 matrix of floating point integers
    // m : a 1x1 matrix of floating point integers
    // k : a 1x1 matrix of floating point integers, the multiplicative order of a modulo m
    //
    // Description
    // Returns the multiplicative order of a modulo m.
    //
    // We assume that number_gcd(a,m)==1 (if not, an error is generated).
	//
    // The multiplicative order of a modulo m is the smallest
    // positive integer k such that
    //
    //<latex>
    //\begin{eqnarray}
    //a^k \equiv 1 \pmod{m}
    //\end{eqnarray}
    //</latex>
	//
	// Hence, on output, we have
	//
	//<screen>
	//number_powermod(a,k,m)==1
	//</screen>
	//
	// Assume that number_gcd(a,m)==1.
	// If number_multorder(a,m)==number_eulertotient(m), then
	// a is a primitive root modulo m.
    //
    // Examples
    // // The order of 3 modulo 10 is 4
    // k = number_multorder( 3,  10)
	// // Check the definition
	// modulo(3.^(1:4),10)
	//
    // // The order of 5 modulo 12 is 2
    // k = number_multorder( 5,  12)
	// // Check the definition
	// modulo(5.^(1:2),12)
	//
	// // See the order of a modulo 17 for a=1,2,...,16
	//m = 17;
	//for a = 1 : m-1
	//    k = number_multorder(a, m);
	//	mprintf("order(%d) (mod %d) = %d\n",a,m,k);
	//end
	// // See that the primitive roots modulo 17 are
	// // precisely the a which have order 16:
	//G = number_searchprimroot ( m , %inf )'
    //
    // Bibliography
    // http://en.wikipedia.org/wiki/Multiplicative_order
    // http://rosettacode.org/wiki/Multiplicative_order#C
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    //
    //

    [lhs, rhs] = argn()
    apifun_checkrhs ( "number_multorder" , rhs , 2 )
    apifun_checklhs ( "number_multorder" , lhs , 0:1 )
    //
    // Check type
    apifun_checktype ( "number_multorder" , a , "a" , 1 , "constant" )
    apifun_checktype ( "number_multorder" , m , "m" , 2 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "number_multorder" , a , "a" , 1 )
    apifun_checkscalar ( "number_multorder" , m , "m" , 2 )
    //
    // Check content
    apifun_checkflint ( "number_multorder" , a , "a" , 1 )
    apifun_checkrange ( "number_multorder" , a , "a" , 1 , -2^53 , 2^53 )
    apifun_checkflint ( "number_multorder" , m , "m" , 2 )
    apifun_checkrange ( "number_multorder" , m , "m" , 2 , -2^53 , 2^53 )
    //
    d = number_gcd(a,m)
    if (d<>1) then
        localstr = gettext("%s: number_gcd(a,m) is not 1.")
        error(msprintf(localstr, "number_multorder"));
    end
    //

    function k = multi_order_p(a, p, e)
        // Compute the multiplicative order of p^e,
        // where p is a prime.
        m = p^e
        t = m / p * (p - 1)
        [ fac , status ] = number_getfactors(t)
        if (~status) then
            localstr = gettext("%s: The factorization of %d^%d failed.")
            error(msprintf(localstr, "multi_order_p" , p , e ));
        end
        len = size(fac,"*")
        for i = 1 : len
            if (number_powermod(a, fac(i), m) == 1) then
                k = fac(i)
                return
            end
        end
        k = 0
    endfunction

    //
    // Proceed...
    [p,e,status] = number_getprimefactors(m)
    if (~status) then
        localstr = gettext("%s: The factorization of %d failed.")
        error(msprintf(localstr, "multi_order" , m ));
    end
    len = size(p,"*")
    k = 1
    for i = 1 : len
        h = multi_order_p(a, p(i), e(i))
        k = number_lcm(k, h)
    end

endfunction
