// Copyright (C) 2012 - Michael Baudin

// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function Z = number_addgroupmod ( m )
    // Returns the additive group modulo m.
    //
    // Calling Sequence
    //   Z = number_addgroupmod ( m )
    //
    // Parameters
    //   m : a 1-by-1 matrix of floating point integers, must be greater or equal than 1.
    //   Z : a m-by-1 matrix of floating point integers, the additive group modulo m.
    //
    // Description
    // The elements of the multiplicative group modulo m are the
    // integers a in the set {0,1,2,...,m-1}.
    // This group is also called the least residue system modulo m.
    //
    // This is a group under addition.
    // In other words, if a and b are in Z, therefore a+b (mod m) is in Z.
    //
    // Examples
    // // See the additive group modulo 10
    // // 0, 1, 2, ..., 9
    // Z = number_addgroupmod(10)
    // // See that this is a group.
    // modulo(7+9,10)
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    //

    [lhs, rhs] = argn()
    apifun_checkrhs ( "number_addgroupmod" , rhs , 1 )
    apifun_checklhs ( "number_addgroupmod" , lhs , 0:1 )
    //
    // Check type
    apifun_checktype ( "number_addgroupmod" , m , "m" , 1 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "number_addgroupmod" , m , "m" , 1 )
    //
    // Check content
    apifun_checkflint ( "number_addgroupmod" , m , "m" , 1 )
    apifun_checkrange ( "number_addgroupmod" , m , "m" , 1 , 1 , 2^53 )
    //
    // Proceed...
    Z = (0:m-1)'
endfunction

