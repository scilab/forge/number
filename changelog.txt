changelog of the Number Scilab Toolbox

number (1.3)
    * Added details in the comments of
      number_pseudoprime, number_coprime, number_probableprime
    * Updated demos.
    * Created number_eulertotient.
    * Created number_isprimroot.
    * Created number_searchprimroots.
    * Created number_multgroupmod.
    * Created number_addgroupmod.
    * Renamed number_minimumprimegap into number_maximalprimegap.
    * Created a "Special Numbers" help section.
    * Moved prime-related functions into a new
      "Prime Numbers" help section.
    * Created number_getfactors.
    * Created number_getprimefactors.
    * Created a "Factorization" help section.
    * Created number_multorder.
    * Created a "Multiplicative Group" help section.
    * Created number_productmod.
    * Changed number_primes1000: now returns a row vector.
    * Extended number_asmodm and number_powermod to
      negative arguments.
    * Simplified "pollard-rho" method of number_factor to
      make it closer to the original algorithm.
    * Improved performance of number_powermod, number_asmodm,
      number_probableprime.
    * Improved performance of number_primitiveroot.
    * Created number_ulamplot.
    * Improved description of number_ulamspiral.

number (1.2)
    * Added Ulam spiral.
    * Moved ismember to specfun module.
    * Added number_multiprimality.
    * Added number_minimumprimegap.
    * Fixed help page of bin2hex, hex2bin.
    * Updated tests to use assert module.
    * Removed demos generated from examples.
    * Use apifun_checkflint.
    * Use apifun_argindefault.

number (1.1)
    * Added bin2hex and hex2bin.
    * Checked arguments of tobary, frombary with apifun.
    * Updated implementation of factor to remove global variables (backward compatibility lost)
    * Updated implementation of isprime to remove global variables (backward compatibility lost)
    * Removed cget, startup, configure to remove global variables.
    * Added more complete example in carmichael.
    * Used apifun in all functions: argument checking is more complete.
    * Extended lcm, gcd, factor and isprime to negative numbers.
    * Added number_primecount
    * make .xml from the txt notes

number (1.0)
    * Added missing number_checkrange
    * Created number_barygui
    * Created number_frombary
    * Renamed number_bary > number_tobary (backward compatibility lost)
    * Fixed bug in number_check.
    * Separated the help pages.
 -- Michael Baudin <michael.baudin@scilab.org>  Sep 2010

number (0.2)
    * Fixed startup script
    * Created number_powermod
    * Created number_check
    * Created number_pseudoprime
    * Created number_probableprime
    * Created number_bary
    * Created number_carmichael
    * Created number_fermat
    * Created number_mersenne
    * Created number_primes1000
    * Created number_inversemod
    * Created number_isdivisor
    * Created number_solvelinmod
    * Created number_extendedeuclid
    * Added a clear implementation of Pollard-rho and removed Pollard-floyd and Pollard-brent
    * Added a warning message when status is false and is not an output argument
 -- Michael Baudin <michael.baudin@scilab.org>  Nov 2009

number (0.1)
    * Initial version
 -- Michael Baudin <michael.baudin@scilab.org>  Nov 2009


