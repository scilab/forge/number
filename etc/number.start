// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function numberlib = loadnumberlib ()

    TOOLBOX_NAME = "number"
    TOOLBOX_TITLE = "Number"
    verbose_at_startup=%f

    mprintf("Start %s\n",TOOLBOX_TITLE);

    etc_tlbx  = get_absolute_file_path(TOOLBOX_NAME+".start");
    etc_tlbx  = getshortpathname(etc_tlbx);
    root_tlbx = strncpy( etc_tlbx, length(etc_tlbx)-length("\etc\") );

    // Load  functions library
    if ( %t ) then
        if (verbose_at_startup) then
            mprintf("\tLoad macros\n");
        end
        pathmacros = pathconvert( root_tlbx ) + "macros" + filesep();
        numberlib  = lib(pathmacros);
    end

    // Load gateways
    if ( %f ) then
        if (verbose_at_startup) then
            mprintf("\tLoad gateways\n");
        end
        ilib_verbose(0);
        exec( pathconvert(root_tlbx+"/sci_gateway/loader_gateway.sce",%f));
    end

    // Load and add help chapter
    if or(getscilabmode() == ["NW";"STD"]) then
        if (verbose_at_startup) then
            mprintf("\tLoad help\n");
        end
        path_addchapter = pathconvert(root_tlbx+"/jar");
        if ( isdir(path_addchapter) <> [] ) then
            add_help_chapter(TOOLBOX_TITLE, path_addchapter, %F);
        end
    end

    // Add demos
    if or(getscilabmode() == ["NW";"STD"]) then
        if (verbose_at_startup) then
            mprintf("\tLoad demos\n");
        end
        demoscript = TOOLBOX_NAME + ".dem.gateway.sce"
        pathdemos = pathconvert(fullfile(root_tlbx,"demos",demoscript),%f,%t);
        add_demo(TOOLBOX_TITLE,pathdemos);
    end

    // A Welcome message.
    if (verbose_at_startup) then
        mprintf("\tType ""help number_overview"" for quick start.\n");
        mprintf("\tType ""demo_gui()"" and search for "+TOOLBOX_TITLE+" for Demonstrations.\n");
    end

endfunction

if ( isdef("numberlib") ) then
    warning("	Library is already loaded (""ulink(); clear numberlib;"" to unload.)");
    return;
end

numberlib = loadnumberlib();
clear loadnumberlib;


